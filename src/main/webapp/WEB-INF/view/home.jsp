<%@ page import="edu.osu.oopr3.entity.UserExtended" %>
<%@ page import="org.springframework.security.authentication.AnonymousAuthenticationToken" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>HOME</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="menuContainer">
            <ul id="menuBox">
                <c:if test="${param.logout == 'successful'}">
                    <li id="menuRow" style="color: dodgerblue">Odhlášení úspěšné!</li>
                </c:if>
                <%
                    Authentication auth = SecurityContextHolder.getContext().getAuthentication();

                    if(auth.isAuthenticated() && ! (auth instanceof AnonymousAuthenticationToken)) {
                        UserExtended userDetails = (UserExtended) auth.getPrincipal();
                        String timeFormatted = String.format("%1$TD", userDetails.getTimeCreated());
                %>
                <li id="menuRow" style="height: 60px;">
                    <span style="float: left; margin-left:15px; margin-top: 15px; color: dodgerblue;"><%=userDetails.getUsername()%></span>
                    <span style="float: right; margin-right:15px; margin-top: 15px; color: dodgerblue;"><%=timeFormatted%></span>
                </li>
                <%
                    }

                    if(auth.getAuthorities()
                            .stream()
                            .anyMatch(x -> (x.getAuthority().equals("ADMIN")))
                            ) {
                    %>
                <li id="menuRow"><a href="${pageContext.request.contextPath}/api/v1/user/users">UŽIVATELÉ</a></li>
                <%
                    }

                    if(auth.isAuthenticated() && ! (auth instanceof AnonymousAuthenticationToken)) {
                %>
                <li id="menuRow"><a href="${pageContext.request.contextPath}/api/v1/actors">HERCI</a><li>
                <%
                    }

                    if(auth.isAuthenticated() && auth instanceof AnonymousAuthenticationToken) {
                %>
                <li id="menuRow"><a href="${pageContext.request.contextPath}/login">PŘIHLÁSIT</a><li>
                <li id="menuRow"><a href="${pageContext.request.contextPath}/registration">REGISTROVAT</a><li>
                <%
                    } else {
                %>
                <li id="menuRow"><a href="${pageContext.request.contextPath}/logout">ODHLÁSIT</a></li>
                <%
                    }
                %>
            </ul>
        </div>
    </body>
</html>
