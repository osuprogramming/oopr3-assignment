<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>ERROR</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="errorContainer">
            <div>
                <p><b> HTTP STATUS <%= request.getParameter("TYPE") %>
                    <c:choose>
                        <c:when test="${param.TYPE == '401'}">
                            UNAUTHORIZED
                        </c:when>
                        <c:when test="${param.TYPE == '403'}">
                            FORBIDDEN
                        </c:when>
                    </c:choose>
                </b></p>
                <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
            </div>
        </div>
    </body>
</html>
