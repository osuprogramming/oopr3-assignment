<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.authentication.AnonymousAuthenticationToken" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>LOGIN</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>

        <%
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if( (auth instanceof AnonymousAuthenticationToken)) {
        %>

        <div id="loginContainer">
            <c:if test="${param.error == 'true'}">
                <p style="color: red">Špatné údaje!</p>
            </c:if>
            <form action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
                <label for="username">Uživatelské jméno</label>
                <input type="text" id="username" name="username" placeholder="Uživatelské jméno"> <br/>
                <label for="password">Heslo</label>
                <input type="password" id="password" name="password" placeholder="Heslo"> <br/>
                <input type="submit" id="loginButton" value="Přihlásit">
            </form>
            <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
        </div>

    <%
            } else {
                response.sendRedirect("/home");
            }
    %>
    </body>
</html>
