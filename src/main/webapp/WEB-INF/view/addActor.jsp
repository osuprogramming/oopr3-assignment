<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>ACTORS</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if( auth.getAuthorities().stream().anyMatch(x -> (x.getAuthority().equals("ADMIN"))) ) {
        %>
        <div id="registrationContainer">
            <form action="${pageContext.request.contextPath}/api/v1/actors/insert" method="POST">
                <c:choose>
                    <c:when test="${param.error == 'FIRST_NAME_INVALID'}">
                        <label for="firstName">*** Jméno ***</label>
                        <input type="text" id="firstName" name="firstName" placeholder="Jméno" style="color: red;">
                    </c:when>
                    <c:otherwise>
                        <label for="firstName">*** Jméno ***</label>
                        <input type="text" id="firstName" name="firstName" placeholder="Jméno">
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'LAST_NAME_INVALID'}">
                        <label for="lastName">*** Příjmení ***</label>
                        <input type="text" id="lastName" name="lastName" placeholder="Příjmení" style="color: red;">
                    </c:when>
                    <c:otherwise>
                        <label for="lastName">*** Příjmení ***</label>
                        <input type="text" id="lastName" name="lastName" placeholder="Příjmení">
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'MIDDLE_NAME_INVALID'}">
                        <label for="middleName">Prostřední jméno</label>
                        <input type="text" id="middleName" name="middleName" placeholder="Prostřední jméno" style="color: red;">
                    </c:when>
                    <c:otherwise>
                        <label for="middleName">Prostřední jméno</label>
                        <input type="text" id="middleName" name="middleName" placeholder="Prostřední jméno">
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'AGE_INVALID'}">
                        <label for="age">*** Věk ***</label>
                        <input type="text" id="age" name="age" placeholder="Věk" style="color: red;">
                    </c:when>
                    <c:otherwise>
                        <label for="age">*** Věk ***</label>
                        <input type="text" id="age" name="age" placeholder="Věk">
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'PAYROLL_INVALID'}">
                        <label for="payroll">*** Hodinová sazba v celých číslech ***</label>
                        <input type="text" id="payroll" name="payroll" placeholder="Hodinová sazba" style="color: red;">
                    </c:when>
                    <c:otherwise>
                        <label for="payroll">*** Hodinová sazba v celých číslech ***</label>
                        <input type="text" id="payroll" name="payroll" placeholder="Hodinová sazba">
                    </c:otherwise>
                </c:choose>
                <input type="submit" id="loginButton" value="Přidat">
            </form>
        </div>
        <%
            } else {
                response.sendRedirect("/error?TYPE=403");
            }
        %>
        <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
        <a id="homeNavigation" href="${pageContext.request.contextPath}/api/v1/actors"><button class="homeButtonContainer">HERCI</button></a>
    </body>
</html>
