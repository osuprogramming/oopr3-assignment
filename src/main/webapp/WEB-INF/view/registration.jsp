<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.authentication.AnonymousAuthenticationToken" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>REGISTRATION</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if( (auth instanceof AnonymousAuthenticationToken)) {
        %>
        <div id="registrationContainer">
            <form action="${pageContext.request.contextPath}/api/v1/user" method="POST">
                <c:choose>
                    <c:when test="${param.error == 'USERNAME_INVALID'}">
                        <label for="username">*** Uživatelské jméno ***</label>
                        <input type="text" id="username" name="username" placeholder="Uživatelské jméno" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="username">*** Uživatelské jméno ***</label>
                        <input type="text" id="username" name="username" placeholder="Uživatelské jméno"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'PASSWORD_INVALID'}">
                        <label for="password">*** Heslo ***</label>
                        <input type="password" id="password" name="password" placeholder="Počet znaků 8-32, velké a malé písmeno, číslo a speciální znak" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="password">*** Heslo ***</label>
                        <input type="password" id="password" name="password" placeholder="Počet znaků 8-32, velké a malé písmeno, číslo a speciální znak"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'MATCHING_PASSWORD_INVALID'}">
                        <label for="matchingPassword">*** Heslo znovu ***</label>
                        <input type="password" id="matchingPassword" name="matchingPassword" placeholder="Počet znaků 8-32, velké a malé písmeno, číslo a speciální znak" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="matchingPassword">*** Heslo znovu ***</label>
                        <input type="password" id="matchingPassword" name="matchingPassword" placeholder="Počet znaků 8-32, velké a malé písmeno, číslo a speciální znak"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'FIRST_NAME_INVALID'}">
                        <label for="firstName">*** Jméno ***</label>
                        <input type="text" id="firstName" name="firstName" placeholder="Jméno" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="firstName">*** Jméno ***</label>
                        <input type="text" id="firstName" name="firstName" placeholder="Jméno"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'LAST_NAME_INVALID'}">
                        <label for="lastName">*** Příjmení ***</label>
                        <input type="text" id="lastName" name="lastName" placeholder="Příjmení" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="lastName">*** Příjmení ***</label>
                        <input type="text" id="lastName" name="lastName" placeholder="Příjmení"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'MIDDLE_NAME_INVALID'}">
                        <label for="middleName">Prostřední jméno</label>
                        <input type="text" id="middleName" name="middleName" placeholder="Prostřední jméno" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="middleName">Prostřední jméno</label>
                        <input type="text" id="middleName" name="middleName" placeholder="Prostřední jméno"> <br/>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${param.error == 'EMAIL_INVALID'}">
                        <label for="email">*** E-mail ***</label>
                        <input type="text" id="email" name="email" placeholder="E-mail" style="background-color: red;"> <br/>
                    </c:when>
                    <c:otherwise>
                        <label for="email">*** E-mail ***</label>
                        <input type="text" id="email" name="email" placeholder="E-mail"> <br/>
                    </c:otherwise>
                </c:choose>
                <input type="submit" id="loginButton" value="Registrovat">
            </form>
            <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
        </div>
        <%
            } else {
                response.sendRedirect("/home");
            }
        %>
    </body>
</html>
