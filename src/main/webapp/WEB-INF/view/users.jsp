<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>USERS</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if( auth.getAuthorities().stream().anyMatch(x -> (x.getAuthority().equals("ADMIN"))) ) {
        %>

        <c:choose>
            <c:when test="${not empty users}">
                <table id="users">
                    <tr>
                        <td>Přezdívka</td>
                        <td>E-mail</td>
                        <td>Jméno</td>
                        <td>Prostřední jméno</td>
                        <td>Příjmení</td>
                    </tr>
                    <c:forEach items="${users}" var="user">
                        <tr>
                            <td>${user.username}</td>
                            <td>${user.email}</td>
                            <td>${user.firstName}</td>
                            <td>${user.middleName}</td>
                            <td>${user.lastName}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:when test="${empty users}">
                <ul id="menuBox">
                    <li id="menuRow">Uživatelé došli!</li>
                </ul>
            </c:when>
        </c:choose>
        <%
            } else {
                response.sendRedirect("/error?TYPE=403");
            }
        %>
        <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
    </body>
</html>
