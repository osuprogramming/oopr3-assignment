<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.authentication.AnonymousAuthenticationToken" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>ACTORS</title>
        <link href="<c:url value="/resources/style.css" />" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            if(auth.isAuthenticated() && ! (auth instanceof AnonymousAuthenticationToken)) {
        %>

        <c:choose>
            <c:when test="${not empty actors}">
                <table id="users">
                    <tr>
                        <td>ID</td>
                        <td>Jméno</td>
                        <td>Prostřední jméno</td>
                        <td>Příjmení</td>
                        <td>Hodinová sazba</td>
                        <td>Věk</td>
                        <td></td>
                    </tr>
                    <c:forEach items="${actors}" var="actor">
                        <tr>
                            <td>${actor.id}</td>
                            <td>${actor.firstName}</td>
                            <td>${actor.middleName}</td>
                            <td>${actor.lastName}</td>
                            <td>${actor.payroll}</td>
                            <td>${actor.age}</td>
                            <td>
                                <%
                                    if(auth.getAuthorities().stream().anyMatch(x -> (x.getAuthority().equals("ADMIN")))) {
                                %>
                                <form action="${pageContext.request.contextPath}/api/v1/actors/delete" method="POST">
                                    <input id="removeButton" type="submit" value="ODSTRANIT">
                                    <input type="hidden" value="${actor.id}" id="actor_id" name="actor_id">
                                </form>
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:when test="${empty users}">
                <ul id="menuBox">
                   <li id="menuRow">Herci došli!</li>
                </ul>
            </c:when>
        </c:choose>
        <%
            } else {
                response.sendRedirect("/error?TYPE=401");
            }
        %>
        <%
            if( auth.getAuthorities().stream().anyMatch(x -> (x.getAuthority().equals("ADMIN"))) ) {
        %>
        <a id="homeNavigation" href="${pageContext.request.contextPath}/api/v1/actors/add"><button class="homeButtonContainer">PŘIDAT</button></a>
        <%
            }
        %>
        <a id="homeNavigation" href="${pageContext.request.contextPath}/home"><button class="homeButtonContainer">DOMŮ</button></a>
    </body>
</html>
