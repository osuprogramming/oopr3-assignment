create table if not exists persistent_logins (
  username varchar(100) not null,
  series varchar(64) primary key,
  token varchar(64) not null,
  last_used timestamp not null
);
/* --;; */
INSERT INTO user(username, email, first_name, last_name, middle_name, password) VALUES(
  'john_doe',
  'john.doe@gmail.com',
  'John',
  'Doe',
  'Middle',
  '$2a$10$o00r/D12pAIiV4pV6Qe9yecYCYeOt21tAvOxCyzlX6hEFcSHdmNhq'    /* johnyDoe*123 */
);

INSERT INTO user(username, email, first_name, last_name, middle_name, password) VALUES(
  'robbert_king',
  'rob.king@yahoo.com',
  'Robby',
  'King',
  'The',
  '$2a$10$QPpNnz5DdwapaLX/g9N5XeweKW8NljQgDau6i08JuDeqHhz5ODrpa'    /* robbyKingo*321 */
);

INSERT INTO user(username, email, first_name, last_name, middle_name, password) VALUES(
  'donny_crusoe',
  'crusoe_donny@bing.com',
  'Crusoe',
  'Donn',
  'Of',
  '$2a$10$D2j6gDCMWVz5hSYnDTBSueqL/hGL8YVX.zeyS4LGfzfwodsJefNp6'    /* crusoeDonn*312 */
);

INSERT INTO user(username, email, first_name, last_name, middle_name, password) VALUES(
  'elizabeth_the_first',
  'eli.the.first@duckduck.com',
  'Elie',
  'First',
  'The',
  '$2a$10$kNWOIJ0FDb3rqPo5TsgucOEIndo.KyhB34/z4u6zsY.wyqYqk2MBK'    /* theEliFirst*132 */
);

INSERT INTO role(text) VALUES(
  'ADMIN'
);

INSERT INTO role(text) VALUES(
  'USER'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '1',
  '1'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '1',
  '2'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '2',
  '1'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '2',
  '2'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '3',
  '2'
);

INSERT INTO user_roles(user_id, role_id) VALUES(
  '4',
  '1'
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  49,
  'Peter',
  'Dinklage',
  17000
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  45,
  'Lena',
  'Headey',
  11000
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  32,
  'Emilia',
  'Clarke',
  12000
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  32,
  'Kit',
  'Harington',
  17000
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  31,
  'Rose',
  'Leslie',
  14000
);

INSERT INTO actor(age, first_name, last_name, payroll) VALUES(
  21,
  'Maisie',
  'Wiliams',
  19000
);