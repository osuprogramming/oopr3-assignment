package edu.osu.oopr3.mapper;

import edu.osu.oopr3.model.User;
import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.model.UserRoles;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserMapper {

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "roles", ignore = true)
    UserDTO map(final User source);

    @Mapping(target = "roles", ignore = true)
    User map(final UserDTO source);

    static UserMapper instance() {
        return Mappers.getMapper(UserMapper.class);
    }
}
