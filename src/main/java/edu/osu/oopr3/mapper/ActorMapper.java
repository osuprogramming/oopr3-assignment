package edu.osu.oopr3.mapper;

import edu.osu.oopr3.entity.ActorDTO;
import edu.osu.oopr3.model.Actor;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ActorMapper {

    ActorDTO map(final Actor source);

    Actor map(final ActorDTO source);

    static ActorMapper instance() {
        return Mappers.getMapper(ActorMapper.class);
    }
}
