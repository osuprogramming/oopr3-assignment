package edu.osu.oopr3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan( basePackages = {
        "edu.osu.oopr3",
        "edu.osu.oopr3.rest.v1.service.*",
        "edu.osu.oopr3.rest.v1.controller",
})
public class OOPR3Boot extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
        return builder.sources(OOPR3Boot.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(OOPR3Boot.class, args);
    }
}
