package edu.osu.oopr3.validation;

public enum UserValidationStatus {
    USERNAME_INVALID,
    PASSWORD_INVALID,
    MATCHING_PASSWORD_INVALID,
    FIRST_NAME_INVALID,
    MIDDLE_NAME_INVALID,
    LAST_NAME_INVALID,
    EMAIL_INVALID,
    VALID
}
