package edu.osu.oopr3.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidation {

    public static boolean validateUsername(String username) {
        Pattern pattern = Pattern.compile("^[a-z0-9][a-zA-Z0-9_-]{1,30}[a-z0-9]$");
        Matcher matcher = pattern.matcher(username.toLowerCase());
        return matcher.matches();
    }

    public static boolean validatePassword(String password) {
        Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,32}$");
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean validateFirstName(String firstName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(firstName);
        return matcher.matches();
    }

    public static boolean validateLastName(String lastName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(lastName);
        return matcher.matches();
    }

    public static boolean validateMiddleName(String middleName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(middleName);
        return matcher.matches() || middleName.isEmpty();
    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
