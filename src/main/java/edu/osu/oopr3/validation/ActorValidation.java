package edu.osu.oopr3.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActorValidation {

    public static boolean validateFirstName(String firstName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(firstName);
        return matcher.matches();
    }

    public static boolean validateLastName(String lastName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(lastName);
        return matcher.matches();
    }

    public static boolean validateMiddleName(String middleName) {
        Pattern pattern = Pattern.compile("^[A-Z]\\p{L}{1,31}$");
        Matcher matcher = pattern.matcher(middleName);
        return matcher.matches() || middleName.isEmpty();
    }

    public static boolean validateAge(String age) {
        Pattern pattern = Pattern.compile("^\\d+$");
        Matcher matcher = pattern.matcher(age);
        return matcher.matches();
    }

    public static boolean validatePayroll(String payroll) {
        Pattern pattern = Pattern.compile("^\\d+$");
        Matcher matcher = pattern.matcher(payroll);
        return matcher.matches();
    }
}
