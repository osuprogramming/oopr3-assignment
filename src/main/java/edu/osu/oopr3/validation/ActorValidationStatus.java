package edu.osu.oopr3.validation;

public enum  ActorValidationStatus {
    FIRST_NAME_INVALID,
    MIDDLE_NAME_INVALID,
    LAST_NAME_INVALID,
    AGE_INVALID,
    PAYROLL_INVALID,
    VALID
}
