package edu.osu.oopr3.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"username"})})
public class User {

    @Id
    @Setter(AccessLevel.NONE)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 3, max = 32)
    @NotNull
    private String username;

    @Size(min = 8, max = 128)
    @NotNull
    private String password;

    @OneToMany(
            cascade = CascadeType.REMOVE,
            fetch = FetchType.EAGER,
            mappedBy = "user",
            orphanRemoval = true
    )
    private List<UserRoles> roles;

    @Size(min = 2, max = 32)
    private String firstName;

    @Size(min = 2, max = 32)
    private String middleName;

    @Size(min = 2, max = 32)
    private String lastName;

    @Size(min = 8, max = 64)
    private String email;

    @Setter(AccessLevel.NONE)
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeCreated;
}
