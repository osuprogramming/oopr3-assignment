package edu.osu.oopr3.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 2, max = 32)
    private String firstName;

    @Size(min = 2, max = 32)
    private String middleName;

    @Size(min = 2, max = 32)
    private String lastName;

    private Long age;

    private Long payroll;
}
