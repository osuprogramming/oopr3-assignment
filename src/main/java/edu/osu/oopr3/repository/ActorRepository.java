package edu.osu.oopr3.repository;

import edu.osu.oopr3.model.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ActorRepository extends JpaRepository<Actor, Long> {

    @Override
    @Transactional(readOnly =  true)
    List<Actor> findAll();

    @Override
    @Transactional
    <S extends Actor> S saveAndFlush(S entity);

    @Override
    @Transactional
    void deleteInBatch(Iterable<Actor> entities);
}
