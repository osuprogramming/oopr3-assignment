package edu.osu.oopr3.repository;

import edu.osu.oopr3.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    @Transactional(readOnly = true)
    User getOne(Long id);

    @Override
    @Transactional(readOnly = true)
    List<User> findAll();

    @Override
    @Transactional
    <S extends User> S saveAndFlush(S entity);

    @Override
    @Transactional
    void deleteInBatch(Iterable<User> entities);

    @Query("SELECT u FROM User u WHERE u.username = ?1")
    @Transactional(readOnly = true)
    User findByUsername(String username);
}
