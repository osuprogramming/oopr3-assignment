package edu.osu.oopr3.repository;

import edu.osu.oopr3.model.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRoles, Long> {

}
