package edu.osu.oopr3.repository;

import edu.osu.oopr3.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Override
    @Transactional(readOnly = true)
    List<Role> findAll();
}
