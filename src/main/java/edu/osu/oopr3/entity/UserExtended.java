package edu.osu.oopr3.entity;

import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.sql.Timestamp;
import java.util.Collection;

@Getter
public class UserExtended extends User {

    private static final long serialVersionUID = -3531439484732724601L;

    private final Timestamp timeCreated;

    public UserExtended(
            String username,
            String password,
            Collection<? extends GrantedAuthority> authorities,
            Timestamp timeCreated) {
        super(username, password, authorities);
        this.timeCreated = timeCreated;
    }

    public UserExtended(
            String username,
            String password,
            boolean enabled,
            boolean accountNonExpired,
            boolean credentialsNonExpired,
            boolean accountNonLocked,
            Collection<? extends GrantedAuthority> authorities,
            Timestamp timeCreated) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.timeCreated = timeCreated;
    }
}
