package edu.osu.oopr3.entity;

import lombok.Data;

@Data
public class ActorDTO {

    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String age;
    private String payroll;
}
