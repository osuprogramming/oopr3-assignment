package edu.osu.oopr3.entity;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {

    private String username;
    private String password;
    private String matchingPassword;
    private List<String> roles;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
}
