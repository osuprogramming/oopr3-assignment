package edu.osu.oopr3.rest.v1.service.impl;

import edu.osu.oopr3.entity.ActorDTO;
import edu.osu.oopr3.mapper.ActorMapper;
import edu.osu.oopr3.model.Actor;
import edu.osu.oopr3.repository.ActorRepository;
import edu.osu.oopr3.rest.v1.service.ActorService;
import edu.osu.oopr3.validation.ActorValidation;
import edu.osu.oopr3.validation.ActorValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActorServiceImpl implements ActorService {

    @Autowired
    private ActorRepository repository;

    private static final ActorMapper MAPPER = ActorMapper.instance();

    @Override
    public List<ActorDTO> get() {
        return repository.findAll().stream().map(MAPPER::map).collect(Collectors.toList());
    }

    @Override
    public ActorValidationStatus insert(ActorDTO actor) {
        ActorValidationStatus status = validateActor(actor);

        if(status != ActorValidationStatus.VALID) {
            return status;
        }

        Actor model = MAPPER.map(actor);

        repository.saveAndFlush(model);

        return status;
    }

    @Override
    public void delete(ActorDTO actor) {
        repository.deleteInBatch(Collections.singletonList(MAPPER.map(actor)));
    }

    private ActorValidationStatus validateActor(ActorDTO actor) {
        if(! ActorValidation.validateFirstName(actor.getFirstName())) {
            return ActorValidationStatus.FIRST_NAME_INVALID;
        }

        if(! ActorValidation.validateMiddleName(actor.getMiddleName())) {
            return ActorValidationStatus.MIDDLE_NAME_INVALID;
        }

        if(! ActorValidation.validateLastName(actor.getLastName())) {
            return ActorValidationStatus.LAST_NAME_INVALID;
        }

        if(! ActorValidation.validateAge(actor.getAge())) {
            return ActorValidationStatus.AGE_INVALID;
        }

        if(! ActorValidation.validatePayroll(actor.getPayroll())) {
            return ActorValidationStatus.PAYROLL_INVALID;
        }

        return ActorValidationStatus.VALID;
    }
}
