package edu.osu.oopr3.rest.v1.service;

import edu.osu.oopr3.model.Role;

public interface SecurityService {

    void authenticate(String username, String password, Role role);

    boolean isAuthenticated();
}
