package edu.osu.oopr3.rest.v1.service;

import edu.osu.oopr3.entity.ActorDTO;
import edu.osu.oopr3.validation.ActorValidationStatus;

import java.util.List;

public interface ActorService {

    List<ActorDTO> get();

    ActorValidationStatus insert(ActorDTO actor);

    void delete(ActorDTO actor);
}
