package edu.osu.oopr3.rest.v1.service.impl;

import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.mapper.UserMapper;
import edu.osu.oopr3.model.Role;
import edu.osu.oopr3.model.User;
import edu.osu.oopr3.model.UserRoles;
import edu.osu.oopr3.repository.RoleRepository;
import edu.osu.oopr3.repository.UserRepository;
import edu.osu.oopr3.repository.UserRoleRepository;
import edu.osu.oopr3.rest.v1.service.SecurityService;
import edu.osu.oopr3.rest.v1.service.UserService;
import edu.osu.oopr3.validation.UserValidation;
import edu.osu.oopr3.validation.UserValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private SecurityService service;

    private static final UserMapper MAPPER = UserMapper.instance();

    @Override
    public List<UserDTO> get() {
        List<User> users = repository.findAll();
        return users
                .stream()
                .map(MAPPER::map)
                .collect(Collectors.toList());
    }

    @Override
    public UserValidationStatus insert(UserDTO user) {
        UserValidationStatus status = validateUser(user);

        if(status != UserValidationStatus.VALID) {
            return status;
        }

        List<Role> roles = roleRepository
                .findAll()
                .stream()
                .filter(x -> x.getText().equals("USER"))
                .collect(Collectors.toList());

        User model = MAPPER.map(user);

        UserRoles userRole = new UserRoles();
        userRole.setUser(model);
        userRole.setRole(roles.get(0));

        List<UserRoles> userRoles = new ArrayList<>();
        userRoles.add(userRole);
        model.setRoles(userRoles);

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encryptedPassword = bCryptPasswordEncoder.encode(model.getPassword());
        model.setPassword(encryptedPassword);

        repository.saveAndFlush(model);
        userRoleRepository.saveAll(userRoles);
        userRoleRepository.flush();

        service.authenticate(user.getUsername(), user.getPassword(), model.getRoles().get(0).getRole());

        return status;
    }

    private UserValidationStatus validateUser(UserDTO user) {
        if(! UserValidation.validateUsername(user.getUsername())) {
            return UserValidationStatus.USERNAME_INVALID;
        }

        if(! UserValidation.validatePassword(user.getPassword())) {
            return UserValidationStatus.PASSWORD_INVALID;
        }

        if(! user.getPassword().equals(user.getMatchingPassword())) {
            return UserValidationStatus.MATCHING_PASSWORD_INVALID;
        }

        if(! UserValidation.validateEmail(user.getEmail())) {
            return UserValidationStatus.EMAIL_INVALID;
        }

        if(! UserValidation.validateFirstName(user.getFirstName())) {
            return UserValidationStatus.FIRST_NAME_INVALID;
        }

        if(! UserValidation.validateMiddleName(user.getMiddleName())) {
            return UserValidationStatus.MIDDLE_NAME_INVALID;
        }

        if(! UserValidation.validateLastName(user.getLastName())) {
            return UserValidationStatus.LAST_NAME_INVALID;
        }

        return UserValidationStatus.VALID;
    }
}
