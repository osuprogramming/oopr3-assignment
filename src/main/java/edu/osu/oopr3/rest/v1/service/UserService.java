package edu.osu.oopr3.rest.v1.service;

import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.validation.UserValidationStatus;

import java.util.List;

public interface UserService {

    List<UserDTO> get();

    UserValidationStatus insert(UserDTO user);
}
