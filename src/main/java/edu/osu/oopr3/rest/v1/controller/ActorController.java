package edu.osu.oopr3.rest.v1.controller;

import edu.osu.oopr3.entity.ActorDTO;
import edu.osu.oopr3.rest.v1.service.ActorService;
import edu.osu.oopr3.rest.v1.service.SecurityService;
import edu.osu.oopr3.validation.ActorValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/v1/actors")
public class ActorController {

    @Autowired
    private ActorService service;

    @Autowired
    private SecurityService securityService;

    @GetMapping
    public String getAllActors(final Model model) {
        model.addAttribute("actors", service.get());
        return "actors";
    }

    @PostMapping("/insert")
    public String insertActor(
            @RequestParam("firstName") String firstName,
            @RequestParam("middleName") String middleName,
            @RequestParam("lastName") String lastName,
            @RequestParam("age") String age,
            @RequestParam("payroll") String payroll
    ) {
        if(! securityService.isAuthenticated()) {
            return "redirect:/error?TYPE=403";
        }
        ActorDTO actor = new ActorDTO();
        actor.setFirstName(firstName);
        actor.setMiddleName(middleName);
        actor.setLastName(lastName);
        actor.setAge(age);
        actor.setPayroll(payroll);
        ActorValidationStatus status = service.insert(actor);
        if(status != ActorValidationStatus.VALID){
            return "redirect/api/v1/actors/add?error=" + status.name();
        }
        return "redirect:/api/v1/actors";
    }

    @PostMapping("/delete")
    public String deleteActor(@RequestParam("actor_id") final Long actorId) {
        if(! securityService.isAuthenticated()) {
            return "redirect:/error?TYPE=403";
        }
        ActorDTO actor = new ActorDTO();
        actor.setId(actorId);
        service.delete(actor);
        return "redirect:/api/v1/actors";
    }
}
