package edu.osu.oopr3.rest.v1.service.impl;

import edu.osu.oopr3.model.Role;
import edu.osu.oopr3.rest.v1.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    AuthenticationManager authManager;

    @Override
    public void authenticate(String username, String password, Role role) {
        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(role.getText()));
        UsernamePasswordAuthenticationToken authReq
                = new UsernamePasswordAuthenticationToken(username, password, authorities);
        Authentication auth = authManager.authenticate(authReq);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
    }

    @Override
    public boolean isAuthenticated() {
        if(
                SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                //when Anonymous Authentication is enabled
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken) &&
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                        .stream()
                        .anyMatch(x -> (x.getAuthority().equals("ADMIN")))
                ) {
            return true;
        } else {
            return false;
        }
    }
}
