package edu.osu.oopr3.rest.v1.controller;

import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.rest.v1.service.UserService;
import edu.osu.oopr3.validation.UserValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("api/v1/user")
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/users")
    public String getAllUsers(final Model model) {
        model.addAttribute("users", service.get());
        return "/users";
    }

    @PostMapping
    public String registration(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("matchingPassword") String matchingPassword,
            @RequestParam("email") String email,
            @RequestParam("firstName") String firstName,
            @RequestParam("middleName") String middleName,
            @RequestParam("lastName") String lastName) {
        UserDTO user = new UserDTO();
        user.setUsername(username);
        user.setMatchingPassword(matchingPassword);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        user.setEmail(email);
        UserValidationStatus status = service.insert(user);
        if(status != UserValidationStatus.VALID) {
            return "redirect:/registration?error=" + status.name();
        }
        return "/home";
    }
}
