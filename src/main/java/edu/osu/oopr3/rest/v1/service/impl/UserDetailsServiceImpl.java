package edu.osu.oopr3.rest.v1.service.impl;

import edu.osu.oopr3.entity.UserExtended;
import edu.osu.oopr3.model.User;
import edu.osu.oopr3.model.UserRoles;
import edu.osu.oopr3.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<User> users = repository.findAll();
        User user = users
                .stream()
                .filter( x -> x.getUsername().toLowerCase().equals(username.toLowerCase()) )
                .findFirst()
                .orElse(null);
        if( user == null ) {
            throw new UsernameNotFoundException("User - {" + username + "} was not found in the database");
        }

        List<UserRoles> userRoles = user.getRoles();
        List<GrantedAuthority> grantList = new ArrayList<>();

        if( userRoles != null && ! userRoles.isEmpty() ) {
            userRoles.forEach(x -> grantList.add(new SimpleGrantedAuthority(x.getRole().getText())));
        }

        UserExtended userDetails = new UserExtended(
                username,
                user.getPassword(),
                true,
                true,
                true,
                true,
                grantList,
                user.getTimeCreated()
        );

        return userDetails;
    }
}
