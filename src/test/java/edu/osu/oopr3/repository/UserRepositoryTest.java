package edu.osu.oopr3.repository;

import edu.osu.oopr3.Data;
import edu.osu.oopr3.RepositoryTestConfig;
import edu.osu.oopr3.model.User;
import edu.osu.oopr3.model.UserRoles;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserRepositoryTest extends RepositoryTestConfig {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Test
    public void getAll() {
        assertThat(repository.findAll()).isNotEmpty();
    }

    @Test
    public void getUserRoles() {
        List<User> users = repository.findAll();
        assertThat(users).isNotEmpty();
        assertThat(users.get(0).getRoles().size()).isEqualTo(2);
        assertThat(users.get(0).getRoles().get(0).getRole().getText()).isEqualTo("ADMIN");
        assertThat(users.get(0).getRoles().get(1).getRole().getText()).isEqualTo("USER");
    }

    @Test
    public void deleteUser() {
        User user = Data.getUser();

        List<UserRoles> userRoles = repository.findByUsername(user.getUsername()).getRoles();

        userRoleRepository.deleteInBatch(userRoles);
        repository.deleteInBatch(Collections.singletonList(repository.findByUsername(user.getUsername())));

        assertThat(repository.findByUsername(user.getUsername())).isNull();
        for( UserRoles role : userRoles ) {
            assertThat(userRoleRepository.findById(role.getId())).isEmpty();
        }
    }
}
