package edu.osu.oopr3.repository;

import edu.osu.oopr3.RepositoryTestConfig;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class RoleRepositoryTest extends RepositoryTestConfig {

    @Autowired
    private RoleRepository repository;

    @Test
    public void getAll() {
        assertThat(repository.findAll()).isNotEmpty();
    }
}
