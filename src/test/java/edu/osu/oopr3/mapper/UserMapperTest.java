package edu.osu.oopr3.mapper;

import edu.osu.oopr3.Data;
import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.model.User;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserMapperTest {

    private static final UserMapper MAPPER = UserMapper.instance();

    @Test
    public void mapToDTO() {
        User source = Data.getUser();
        UserDTO target = MAPPER.map(source);
        compare(source, target);
        assertThat(target.getPassword()).isNull();
    }

    @Test
    public void mapToDBO() {
        UserDTO source = Data.getUserDTO();
        User target = MAPPER.map(source);
        compare(target, source);
        assertThat(source.getPassword()).isSameAs(target.getPassword());
    }

    private void compare(User user, UserDTO userDTO) {
        assertThat(user.getUsername()).isSameAs(userDTO.getUsername());
        assertThat(user.getFirstName()).isSameAs(userDTO.getFirstName());
        assertThat(user.getMiddleName()).isSameAs(userDTO.getMiddleName());
        assertThat(user.getLastName()).isSameAs(userDTO.getLastName());
        assertThat(user.getEmail()).isSameAs(userDTO.getEmail());
    }
}
