package edu.osu.oopr3;

import edu.osu.oopr3.entity.UserDTO;
import edu.osu.oopr3.model.User;

public class Data {

    public static User getUser() {
        User user = new User();
        user.setUsername("john_doe");
        user.setPassword("johnyDoe*123");
        user.setFirstName("John");
        user.setMiddleName("Middle");
        user.setLastName("Doe");
        user.setEmail("john.doe@gmail.com");
        return user;
    }

    public static User getInvalidUser() {
        User user = new User();
        user.setUsername("j1");
        user.setPassword("johndoe");
        user.setFirstName("j");
        user.setMiddleName("d");
        user.setLastName("o");
        user.setEmail("@.com");
        return user;
    }

    public static UserDTO getUserDTO() {
        UserDTO user = new UserDTO();
        user.setUsername("john_doe");
        user.setPassword("johnyDoe");
        user.setFirstName("John");
        user.setMiddleName("Middle");
        user.setLastName("Doe");
        user.setEmail("john.doe@gmail.com");
        return user;
    }
}
