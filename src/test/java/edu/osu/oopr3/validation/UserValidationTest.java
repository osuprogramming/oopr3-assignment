package edu.osu.oopr3.validation;

import edu.osu.oopr3.Data;
import edu.osu.oopr3.model.User;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UserValidationTest {

    @Test
    public void wrongData() {
        compare(Data.getInvalidUser(), false);
    }

    @Test
    public void validData() {
        compare(Data.getUser(), true);
    }

    private void compare(User user, boolean validity) {
        assertThat(UserValidation.validateUsername(user.getUsername())).isEqualTo(validity);
        assertThat(UserValidation.validatePassword(user.getPassword())).isEqualTo(validity);
        assertThat(UserValidation.validateFirstName(user.getFirstName())).isEqualTo(validity);
        assertThat(UserValidation.validateLastName(user.getLastName())).isEqualTo(validity);
        assertThat(UserValidation.validateMiddleName(user.getMiddleName())).isEqualTo(validity);
        assertThat(UserValidation.validateEmail(user.getEmail())).isEqualTo(validity);
    }
}
